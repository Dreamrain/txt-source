「──我不是說過嗎？」

對於匆忙被送到理查德帳篷的傳令，副官內爾瑪大叫。就尖銳的犬牙在嘴角露出來，像是不像隱藏感情一樣，粗暴地大聲吼道。

情不自禁，拿著傳令的士兵露出害怕的表情站了起來。好像感到自己被責備了一樣。

──大聖教軍的義勇兵，為了大義而在周圍的村落掠奪。

直截了當地說，帶來的報告只有這個。那是內爾瑪最害怕的消息，並且甚至還預見到了。內爾瑪嘴裡忌諱地咂著嘴。

這是完全明白的事，義勇兵這種東西，根本不具備偉大的意志和宗教的使命感。那只是為了消除每天被壓抑的郁憤，為了擺脫痛苦的日常生活而拿起槍的一群人。
他們自稱義勇兵，終歸和野獸一樣。如果肚子餓了的話，就會揮舞長槍威脅民眾進行掠奪。這是早就明白的事情，儘管如此，這位名為理查德的將領卻什麼措施也沒做。

內爾瑪眼睛裡可以看到憤怒和輕蔑的情感，那是毫無疑問的神色。理查德一邊阻止她的凝視，一邊向傳令兵宣告了二、三句話，讓其退下。

然後，他一邊從正面看著自己的副官，一邊說。那個老齡特有的沙啞聲音奇妙地在帳幕中回響。

「副官內爾瑪，千人長已經準備好了。在這裡留下最低限度的兵力，除此之外其他人全部帶走進行戰備訓練。」

理查德一邊說著要進行訓練，一邊將手伸向陶器，喝起酒來。
面對這種隨便的態度，內爾瑪幾乎要再次蓬髮怒火。就在這時，她的睫毛突然跳動起來。

正在做準備。那究竟是怎麼回事？
指揮官不可思議的話語瞬間削弱了內爾瑪的氣勢。理查德也慢慢地對他的副官說了，不應該關注那個。

「別想著僅用一支部隊擊潰他們，調動全部的部隊，只需能向新兵們展示戰場到底是怎樣的一個東西就好了。你也沒有什麼實戰經驗吧，隨你的便去試著習慣吧。」

聽到這句話的瞬間，冰冷的感覺從內爾瑪的背脊上升起來。帳篷外傳來宣告出陣準備已完成的鐘聲。

真奇怪。顯然，太快了。

雖說是扎營的狀態，但除了警戒防衛的部隊以外部隊大部分應該還處於修整的狀態。如果讓千人長們去準備行軍的話，花費相應的時間也不奇怪。不管怎麼說，收到義勇兵進行掠奪的報告以後的反應也太快了。為什麼，這麼快隊伍的準備就就緒了？討厭的預感，插在了內爾瑪的心口。

內爾瑪睜大了眼睛，強行把喉嚨扭開一邊放出話來。

「理查德營長。你知道義勇兵會進行掠奪，所以準備出兵嗎？」

內爾瑪不太明白自己應該問什麼。只有那樣的言詞孤零零地從嘴唇漏出了。
理查德一邊將陶器放在桌子上，一邊輕易地回答了那個問題。

「當然了。除此之外，還有什麼能進行良好的訓練嗎？」

指揮官的這句話，就像是讓內爾瑪腦髓麻痺了一樣，腦海中浮現出雪白的色彩。眼皮像痙攣似地顫動著。

這個指揮官，是在理解義勇兵遲早會襲擊村落的基礎上，容忍他們。然後在他們真的爆發的時候，將義勇兵當成新兵的討伐對象，進行類似出擊訓練的東西。
有那麼愚蠢的事嗎？

內爾瑪的思考動搖得至今還不能總結出語言。儘管如此，她還是被必須說些什麼的義務感襲擊，發出了聲音。

「⋯⋯失去民眾的信任。」

與平時堅強的聲音相反，內爾瑪用顫抖的聲音說道。理查德毫不猶豫地回答了那個問題。

「放心吧。我多次宣傳了，周圍村落有自稱義勇兵的紋章教部隊出沒。而且義勇兵的打扮和我軍的打扮並不像。一般不會被認為是同伴吧。」

那個老將的口氣，似乎是輕鬆地說著什麼理所當然的事，甚至有詢問內爾瑪有這麼奇怪嗎的感覺。

理查德用嘶啞的聲音說，即使把義勇兵帶到戰場上也只是礙事。
內爾瑪用力咬住自己的嘴唇。犬牙咬進肉裡好像要流血了。

我也理解那件事，把義勇兵這種只會鬧騰的存在帶入戰場，戰果是不可能上升的。倒不如他們因為膽怯而逃跑的話，反而會引發士氣的混亂。我同意在某處必須與他們分離的意見。

但是，理查德這也是一種做法，一種傷害人民的做法。
當內爾瑪要吐出帶有熱量的話語的瞬間，理查德說話了，似乎要把她的勢頭擊碎。

「好吧，內爾瑪。這是我方能夠獲利的做法。如果要進行戰役，那麼最好是和周圍的村落建立良好的關係。」

一邊聽著理查德的話，內爾瑪睜大眼睛，抬起頭。對於現在自己的臉上貼著什麼樣的表情，內爾瑪也搞不明白。

「一開始就把義勇兵與我軍分離，他們看見後就會對義勇兵產生反感。無論什麼事情，要善於處理才是最重要的。」

這樣一來，村落的好意，新兵的訓練，還有義勇兵們的處分，都可以拿到手中。老將一邊把酒灌到喉嚨裡，一邊說道：「我覺得這是個好辦法。」而且，與不進行訓練，就此戰役敗北相比，受害要小得多。

內爾瑪想發出什麼聲音。想把心中七上八下的驚心動魄的感情發泄到眼前的指揮官身上。
但是，卻找不到為此而說的話。因為自己的無所作為，眼中似乎浮現了淚水。對於內爾瑪來說，理查德說的話一點都不順眼，希望自己能反對他。但儘管如此，自己卻沒有做這些的知識和經驗。

因此，嘴唇顫動，說出一句話已經是讓內爾瑪竭盡全力的事。

「理查德營長。」

什麼呀，理查德粗略地回答了。本該說的話已經說了，卻連對著內爾瑪的視線都不做。如果那樣也沒關係，內爾瑪竭盡全力發出聲音。

「我鄙視你。但是⋯⋯我會遵從命令。」

除此之外，內爾瑪還有很多話沒說。

無法從心底跟隨理查德。所以，我鄙視他。但是，糾正這一切的力量自己卻沒有。所以只能遵從。

什麼呀，真難看。什麼啊，真是無力啊。連自己認為正確的事情都無法用語言表達。內爾瑪被勒死自己的衝動所襲擊，背向指揮官離開了帳篷。眼睛裡閃耀的液體，浮現的表情中包含著無數的感情。

理查德看著她的背影，微微眯起眼睛，透出呼吸。那個腦海裏已經不在考慮義勇兵的事了，而是思考著今後必須發送的書信的事情。

向自治都市菲洛絲，和身處紋章教軍中的以前的學生所寫的書信一事。