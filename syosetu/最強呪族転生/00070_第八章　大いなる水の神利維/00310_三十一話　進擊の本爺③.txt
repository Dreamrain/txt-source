「所以說，我是認為這是理解方法上的錯誤。澤雷麥依姆所提倡的假想惡魔——微觀上的惡魔的存在的誤解，打個誇張的比方，即是人類在構築語言的含義和魔術理論的時候，也可以說是在日常生活中，輕視了對微觀尺度的理解和思考，這便是引起誤解的原因」


我在梅婭和本拉多面前解釋澤雷麥依姆的假想惡魔的悖論。
本拉多頻頻點頭，手飛快地在紙上書寫著魔術式和亞雷文字。

在他的旁邊，梅婭露出呆然的表情。


 「不，怎麼說也太過誇張……不，但是，原來如此！這樣考慮的話，道理就說得通了，麼……？果然，亞貝爾大人具備了超越神代的創始者、利維伊神的頭腦……！」


 「這是理所當然，各種語言、概念都是以我們在宏觀上的理解為前提。由於在魔術方面也是同樣的思維，概念就產生了漏洞。澤雷麥依姆徹底地突破了這個障礙，直白指出了概念中自圓其說而被遺漏的部分，以次元和空間為例證，提倡悖論的說法」


 「但是這樣一來，與現有的對假想惡魔的悖論的解釋實在是相差太大……。笨拙如我，現在完全難以接受這種說法。對於假想惡魔的悖論，有樹形圖的平行世界理論作為依據，衍生了潘多拉盒裡的奈亞菈的思考實驗，以及許多基礎性的理論，居然要將這些完全否定……！」


澤雷麥依姆的假想惡魔的悖論，說白了即是基於精靈體的最小單位精靈體子的特性，假設發生了以幾個精靈體子構成的超小型微觀惡魔的時候所產生的矛盾，以此對惡魔的性質進行推測。

澤雷麥依姆還活著的時候，主流觀點認為，那假想的具有精神機能的微觀惡魔作為精靈體過於簡單，這種架空存在的微觀惡魔不會自主發生，這是不可能的，並且嘲笑這個說法本身就是荒唐滑稽。


說起來澤雷麥依姆過世後經過了一百年以上，主流說法變成了「考慮到魔術理論·精靈理論一直以來的發展，拋開不管發生概率天文級別低的非現實事件，這種存在成立的說法很自然」。

澤雷麥依姆所說是正確的，在百年的發展最後得到了證實。
即使人們都知道直接否認澤雷麥依姆是毫無道理的，但單單讓世人追上他的一部分價值觀也需要大量時間。


 「所以粗略地講，當然只有這個還不夠，結論就是從澤雷麥依姆的假想惡魔的悖論出發，‘關於澤雷麥依姆的微觀狀態的精靈體方程式’只是單純的變換思維方式的根本錯誤，並沒有其他的意思。如果不是極小尺度的精靈體具有變化的性質，那就不會受到上次元和其他世界的干涉了」


 「噢，噢噢，噢噢噢噢噢！如此美妙……！我、我現在簡直是看到了萬物的真理本身！令人羞恥、令人悲傷的是，我沒有能完全理解這一切的聰明才智……！」


本拉多留下了感動的淚水，他的臉突然悲痛地扭曲，跪在地上開始哇哇地哭起來。


 「沒關係的本爺。你想不想站在我的身旁，給予我輔助呢」


 「亞貝爾大人、亞貝爾大人啊——！」


本拉多緊緊地抱住我。
我稍微有點驚訝，但也回抱住本拉多矮小的身體。


 「亞貝爾大人！我，萬分抱歉，不勝感激……！」


我撫著本拉多的頭。
有一個這麼熱心的徒弟，琺基領也安泰了。
這樣一來，我短暫離開這裡也不會有什麼問題吧。


 「額，那個，梅婭、梅婭也明白的噢！也就是說，澤雷麥依姆先生將錯就錯地拋出誰都無法理解的誇張說法後撒手不管，直到現在為止除了亞貝爾以外沒人能夠提出如此像樣的解釋，是這樣吧！」


 「呃？啊，嘛，直截了當地講，也並非不是如此……麼？極端地說各種說法有著各種結論，都會有這樣思維方式出錯的一面，但我對此毫不關心，在考慮極小精靈體的場合直接定位到變換式裡不就好了嗎……」


 「連魔術學的歷史都不知道的小女孩，僅僅是聽說了概括的結論，就在那信口開河！你的發言，輕慢了澤雷麥依姆，侮辱了從格雷姆學者斐那契開始，為熱球的研究獻出整個人生的迪迦南，數式的魔術師葛爾涅，侮辱了現今所有學習魔術的賢者、學者、鍊金術師，侮辱了偉大的亞貝爾大人啊！非常抱歉，亞貝爾大人！請將這個小女孩趕出這個地方！」


本拉多的臉突然變成猿猴一樣扭曲，他揪住梅婭。
我壓住他的肩膀，全力把他拉開。


 「等，等一下，喂！請停下來！本爺快停下！不，你給我停下！」


 「但是、但是亞貝爾大人！剛才這傢伙居然說了那種話！那是無法容忍的！她必須要撤回剛才對亞貝爾大人那淺薄不敬的發言，再五體投地謝罪三天三夜！」


不行了，他停不下來。
這傢伙看起來那麼矮小、弱不禁風，意外的很有力氣。


 「對對、對不起，對不起啊亞貝爾！梅婭、梅婭只是在旁邊聽著太寂寞了，只是想稍微說上話而已，那個，那個……」


梅婭當場跪到地上，肩膀微微抖動，含著淚委屈地說著。


 「啊啊！你不用這樣低頭啊！」


 「‘只是’？這就是你事到如今對自己過錯的辯解嗎！你到底、到底要對亞貝爾大人愚弄到什麼程度！？不行，亞貝爾大人，怎麼可以允許身邊有這樣的傢伙！而且我認為迪恩拉特王國本來就對魔術不尊重！要、要不這樣吧，請您與我一起去瑪哈拉努王國！在那裡有很多我個人認可的魔術師！呆在這種地方，亞貝爾大人也很沒面子的吧！」


 「稍微安靜一下！我真的會把你踢出去的噢！」


 「亞、亞貝爾大人！萬分抱歉！但、但是，我是為亞貝爾大人著想才……！」

---

在這個極其混亂的大廳的角落，一個人、或者說一個精靈體，鍊金術師團副團長雅爾塔米亞進來了。

她看到我在扶起梅婭的身體，本爺緊抱住我的腳的情景後，一副打心底裏無語的表情嘆息起來。


 「你們在幹什麼啊……。亞貝爾、你又把梅婭弄哭了……之前那個綠色頭髮的女生也說了，梅婭你也差不多該放棄這人了吧」


 「哎、哎呀，我只是打算稍微，半認真地和本爺聊聊而已……」


 「剛剛我在遠處看了三十分鐘以上，你們聊的真火熱呢？」


 「是的……」


真是無以辯解。
我不禁垂下頭。


 「反正又是在說無聊的話題吧。趕緊說完，不如說現在就結束」


 「是的……」


我只能點頭。


 「雅爾塔小姐……」


梅婭哭的紅紅的眼睛看著雅爾塔米亞。
雅爾塔米亞再次嘆息。


 「等一下女人！你說什麼是無聊的話題！？我、我和亞貝爾大人討論的是，對澤雷麥依姆的假想惡魔的悖論的解釋啊！這對於魔術學有著多麼大的意義，你這種程度的人怎麼會明白！擅自插手終止話題就算了，但居然當面說這種話，我不得不反駁！」


雅爾塔米亞的肩膀忽地抖了抖。


 「……澤、澤雷麥依姆的？亞貝爾的解釋？」


 「是！二流的鍊金術師要是聽說過這個就怪了呢！哼，看在亞貝爾大人的寬容的份上，這次就不計較吧。不過女人，下不為例！」


 「……稍微、就是稍微，讓我聽聽概要行不行？」


雅爾塔米亞露骨地假裝出沒有興趣的樣子，眼神游離地看向斜上方，指尖梳理著頭髮。


 「雅爾塔小姐！？」


梅婭像是在哀求一樣搖晃著雅爾塔米亞的肩膀。


 「就、就說幾句話！一分鐘！三十秒！不、四十秒！」
