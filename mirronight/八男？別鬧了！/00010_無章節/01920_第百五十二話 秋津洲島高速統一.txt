第百五十二話 秋津洲島高速統一


「要盡全力工作！」

「因為你們無謀的戰鬥讓領地荒廢的債，就由你們加倍開墾領地來償還。既然是難得的魔力，就好好拿去幫忙增加生產力吧」

「「「我們知道了！」」」

在這個世界擁有織田信長、武田信玄、上杉謙信的這三名少女，在我們這些擁有龐大魔力的魔法使面前屈服了。

由於她們犯下了為了統一天下而引發動真格犧牲的戰爭，所以三人都被強制從當家的位子上引退。

這裡面也有【她們三人本就是靠造反才爬上當家的位子，所以現在還是離開自家的好】的理由在裡面。

織田家由北條家庇護的信長的父親織田信秀暫時坐回家主，然後再讓信長的弟弟——|名為信勝的少年繼承這個家本來的傳承家主名信秀成為新家主。

上一代的信秀暫時擔當輔佐信勝的職責。

織田家的新傳承家主名信長就這麼如夢幻般消失，信長變回了織田吉子。

武田家那邊也是，由北條家庇護的信虎成為舊武田領的臨時代官。

信玄的弟弟信繁已經繼承了武田家家主一直以來所用的傳承家主名信虎，並決定不會再次使用新的傳承家主名信玄。

所以現在，信玄也僅僅是武田晴美而已。

上杉家也是同樣情況，由繼承了前代家主為景之名的竜子的表兄擔當了上杉家家督。

謙信之名從此作為禁忌對待，謙信變回了上杉竜子。

被自己老家放逐的這三人，現在正在導師的監視下使用魔法開墾領地作為懲罰。

復興和開發因為戰爭而荒廢的東部地域這個責任，得讓她們好好負起來才行。

她們想外逃的話，有負責監視的導師在。

而且，就算她們不靠魔導飛行船架勢小船出海外逃，不過是區區中級魔法使的三人中途肯定會變成海竜的餌食。

這個島的周邊海域裡存在大量海竜巢穴這件事已經確認了。

以這三人的實力，她們再怎麼努力一次也就能打倒十匹左右的海竜。

這種程度的戰果，只會導致她們成為因同伴被殺而狂怒的海竜們的腹中餐。

這座島會超過一萬年不和外部交流是有理由的。

以她們的魔力搶到一艘魔導飛行船也無法運作，另外這三人逃到其他地方也無法保證自己能好好生活下去。

所以，她們只能在導師的監視下老老實實開墾土地。

至於她們雖擅長攻擊魔法應用方面卻不成熟的毛病，將靠接受布蘭塔克先生等人的指導來解決。

「鮑麥斯特伯爵大人，我們其實更擅長戰鬥……」

「平定南部時請務必讓我們出戰！」

「戰爭才是我的亮點所在」

「不行。你們幾個招惹了太多東部諸侯的怨恨。所以現在最優先該做的是復原那些因戰爭荒廢的場所」

這三人因為被稱為DQN類型的人，所以才能在這座迄今為止因為有關規則無法統一的島上掀起真正的侵略戰爭。

織田家奪走了今川家、水野家、松平家的領地，武田家也奪走了村上家、諏訪家、木曾家的領地。

上杉家那邊也是將反抗自己家臣的領地，以及神保家、畠山家的領地奪走了。

期間還產生了犧牲者，導致這三家的新家主現在都處於顏面無光的狀態。

因為這個緣故，東部地域的總代官一職就由庇護並包養了那些領地被奪走領主們的北條氏康擔當，傷好回歸的今川義元負責輔佐他。

被庇護的領主們也很高興可以作為代官重返原籍。

說不定也會有人因為不能再做領主而不滿，但連那三個DQN都在魔法上被壓倒無法忤逆我了，所以應該不會有人搞不清現在的現實是怎樣吧。

而且在對於這些人來說，比起我那三個DQN還更遭他們怨恨些。

也有人提議將那三個DQN處刑，但我還是決定讓她們活下去負責開發作業。這麼一來對鮑麥斯特伯爵家的不滿和怒火也會被那三人擔過去，我統治起來就輕鬆多了。

再說讓她們去擔當復興和開發的工作還有利益可賺，會這麼思考的我也很有貴族風範了嘛。

「另外，雖然這麼說不太好聽，但她們也算不上什麼戰力呢」

「沒錯。我家的上級魔法使太多了啊」

莉薩認為這三人的實力也就是普通的中級魔法使而已。

雖然在這座島上會被視為天才，但去琳蓋亞大陸的話就會被上級魔法使們壓倒，在魔族之國就只是個普通人。

領主階級的血統會遺傳魔力雖是這座島獨有的特點，但經過漫長年月後遺傳的魔力會發生劣化，即便能遺傳成功魔力的平均水平也變了成初級。

說不定就是因為這樣，他們才會產生渴望來自外部的刺激的心情吧。

秋津洲島的住民會老實的接受異民族的鮑麥斯特伯爵家支配，主要就是因為我家有很多上級魔法使。

這裡已經數百年沒有出現能夠打穿黑硬石巖層挖掘出水井的魔法使，所以他們都很害怕出現水量不足問題嚴峻化的危機。

就在此時可以挖出新水井的我們登場了，又沒有實施什麼惡政，所以我們的統治會被接受也是理所當然的。

另外也有從此可以和外部進行貿易，島民可以移民到島外的好處。

「噢噢———！外部的魔法使居然厲害到這個程度———！」

除了那三人外，還有其他人幫忙開墾。

雖然是DQN做派但那三人也有一些支持者，這些人也被他們的主家放逐了。

所以柴田勝家、武田四天王、小島彌太郎以及其他十幾名初級魔法使，現在也在這裡幫忙開墾。

其他那些人里，有很多如前田利家、丹羽長秀、瀧川一益、武藤喜兵衛、柿崎景家之類我好像在哪裡聽過名字的傢伙。

「話說回來，必須得時刻監視著這點還真是麻煩啊」

「沒錯呢。不然監視一放鬆他們就會鬧起來，過後再去壓制起來也很麻煩」

總之，現在就用開墾的工作來榨取這些人的魔力讓他們沒閑心去思考多餘的事情。

不過，不僅是導師連莉薩都被拉來負責監視導致我們挖掘水井的速度下降了，真讓人頭疼。

「你看吧，信玄。就因為你老是使用陰濕的手段，所以我們才得不到當家大人的信任」

「靠奇襲讓今川義元受了重傷的你資格說別人嗎？」

「要是像我那樣堂堂正正的戰鬥，就不會受到這種評價了」

「才不想被戰鬥笨蛋的謙信這麼說」

「你這傢伙戰鬥時毫不留情太恐怖了啊」

雖然生氣但作業有好好做、原本是敵對關係的這三人，互相聊起來後看上去卻顯得十分要好。

是因為現在都是相同立場，所以她們不用再顧慮多餘事的緣故嗎？

「織田家的家督已經被弟弟信勝搶走了。不過對於那傢伙和老爸來說，現在這種失去領地但有薪水拿的立場真合適吧」

「我家的信繁也是一樣。父親很滿意他呢」

「我對現在的上杉家家督位子沒有什麼特別留戀。就暫時修練魔法等待機會吧」

「你說機會？謙信，難道你還想舉兵？」

「信玄，事到如今就算做那種事也不會有任何結果。我討厭沒有勝算的戰爭」

「那可真是巧了。我也討厭只是輸啊」

你們不會再打什麼鬼主意真是太好了。

這三人都很喜歡勝利，所以不會掀起那種有勇無謀的、只為讓人看看自己最後骨氣的叛亂。

「信長，你又有什麼想法？」

「很簡單」

「簡單？」

「這座島還沒有平定的地方只剩下南部。連那個三好長慶都無法完成的秋津洲島統一大業即將由當家大人完成」

「那些我們當然知道。所以怎麼了？」

信玄也露出【事到如今這種事還用說嗎？】的表情

「冷靜，謙信。那麼，接下來我們需要新的目標吧？也就是！作為女人才能使用的奪取天下的方法。只要我能被當家大人寵愛，再生下當家大人的孩子後讓他擔當這座島的總代官，我就算贏了！」

「也就是模仿秋津洲高臣、細川藤孝、伊達藤子她們的做法嗎……」

信玄似乎已經掌握了這三人盯上了我妻子位子的情報。

這個世界的信玄看來也很擅長收集情報。

只不過就算擅長收集情報她卻發現不到事態有可能會變成現在這樣就是了。

「為了接下來不遭到抵抗，我必須在各種方面磨練女人的本事吶。正好埃莉絲大人她們生下的孩子只會繼承鮑麥斯特伯爵領的本領，或是成為家臣。所以我為了獲得當家大人的寵愛生下他的孩子得好好打磨自己的女人味」

「既然信長有這個打算，那我武田信玄也不能輸了」

「就是說，這是場女人的戰爭呢。我接受了」

我說你們三個，這種商談能不能等我不在時再做？

雖然我本就不打算娶更多女人給自己找麻煩了……。

「莉薩，我，討厭那三個傢伙」

雖然都是相當的美女，但在我的印象裡已經把她們視為DQN三人娘了。

我在前世，只是個認真的普通學生。

所以這三個屬於不良系的人，只會讓我把她們歸納進【從生理上就不擅長應付的女人】範疇中去。

誰也沒法保證她們生下我的孩子後不會立即引起騷動，所以還是暫時和她們拉開距離吧。

「她們是不是需要某種來發泄情緒的場所？」

「或許可以把她們塞到露露出身那作島上的魔物領域去」

成為冒險者每天去狩獵魔物的話，這三人就不會去想什麼多餘的事情了嗎？

至於和她們組隊的隊員，讓那些和她們三個一起被放逐的家臣們擔當就好了吧。

「而且，現在可是絕好的機會啊？」

「是那樣嗎？信長」

「你不懂嗎？謙信。現在當家大然身邊，可是只有老女人莉薩大人哦」

「原來如此！其他女人都不在呢」

「如果我信玄現在能獲得當家大人的寵愛，你們這些不修邊幅的女人都全都會被無視了吶」

「莉薩，冷靜冷靜」

「那三個人，真是有趣呢……」

會被我們聽到還進行這樣會話的這三人也真是厲害。不過我和麗薩是拉開距離後，為了兼顧監視這三人使用魔法偷聽這三人會話的就是了。

被她們說成是老女人的莉薩現在臉上露出了冷酷的笑容。

「信玄這樣發育不足的女人才沒人肯要。肯定是三人中胸部最大，腰最細的我能獲得寵愛吧」

「信長，胸部大小的話，我和你也差不多」

「戰鬥笨蛋的謙信怎麼可能當得了當家大人的對象。現在可是男人向女人尋求治癒的時代」

「信長身上哪裡有能治癒人的部分？要是埃莉絲大人的話還說得通……」

謙信說的沒錯。

我一點也不覺得自己能在這三人身上得到什麼治癒。

「呃，和埃莉絲大人相比的話就連我也得輸。那麼，就成為現在在這裡女子中的第一就好了。只要拿出我的魅力，老女人莉薩大人根本不足為懼」

「如果只要勝過老女人莉薩大人就好，那怎麼可以忘了我的存在？」

「信長和謙信也真可笑，想要勝過老女人莉薩大人的話，這裡不就有個改戰爭天才為女人味天才的武田信玄在嘛」

「……」

「莉薩，冷靜冷靜」

那三人因為以為沒人聽到就毫無顧忌，結果把不得了的事給說出口了。

這下我可不管嘍。

「老公，我失陪一下」

「不要做得太過火哦」

莉薩稍微離開去了別的地方。

一定是去換上過去那身衣服，化過去那個妝了吧。

「唔———嗯，毒就該以毒來壓制！」

幾分鐘後，莉薩以和我們初遇時的扮裝出現了。

因為帶來的沖擊實在太大，信長她們陷入了目瞪口呆動憚不得的狀態。

「你們還真敢給我說個沒完啊！你們幾個，再過十年左右也會變成老女人吧！還敢說這種沒用的閑話，現在可是在懲罰中！給我認真工作！」

對三人怒吼一通後，莉薩隨手向著附近的巨巖放出『冷氣』，將目標從里到外凍了個結實。

她的『冷氣』魔法在參考過我使用的『絕對零度』概念後，威力和以前相比大幅強化。

接著，莉薩向完全凍結的巨巖發射『冰彈』將巨巖打了個粉碎，飛舞到空中的巖石碎渣在陽光下閃閃發亮。

「如果派不上用場還考慮多餘事情，就把你們幾個弄成碎渣然後拿去田裡做出肥料！聽懂了嗎！？」

「「「我們知錯了！」」」

接下來一整天裡，被莉薩的變化嚇到的三人，在莉薩的嚴格監視下一直行進開墾作業直到她們精疲力盡為止。

雖然第二天就變回了平常的樣子，但因為擔心那套打扮不知何時就會再現而忐忑不安的三人，從此以後都老老實實服從莉薩的指示了。

「那三人會有什麼想法，很容易就能想像得到」

「因為被懲罰勞動所以變得容易理解了嗎？」

我把監視DQN三人組的工作交給導師和莉薩，自己使用『瞬間移動』返回了大津城。

現在大津城已經完全變成了鮑麥斯特伯爵家的據點，身為秋津洲島總副代官的雪在這裡處理一切實際政務。

雪雖不是魔法使年齡又比我小一歲，卻是個超級有能力的文官。

甚至把軍隊交給她也能好好指揮，本人又是刀術、槍術、弓術、斧術、薙刀術、馬術、駕船的高手。

這麼有能力的雪按理說本該站出來自立為王的，可她卻只留在青梅竹馬的朋友涼子身邊輔佐，所以在性格上也屬於相當講義氣的類型。

『是個希望她來當在下部下的難得人才啊』

連羅德里希也承認雪這很有能力。

所以即便我把那三人無聊的計劃告訴雪，她也只是露出略微吃驚的表情而已。

「不過，如果當家大人再晚些來到這座島上，她們中的某人說不定真能統一這裡」

「應該是吧」

留在這座島上的話，領主階級遺傳的魔力越來越衰落導致無法生出能挖井的魔法使，外出的話又有海竜礙事，如果再維持現狀繼續下去島上文明衰退的可能會很高。

我覺得那三個DQN正是因為對迄今為止因為戰爭規則而看不到統一徵兆的這座島的未來產生了危機感，所以才做出那種行為的。

如果我不出現的話，那三人將這座島引導進以血洗血的亂世的可能性應該很高吧。

不過，那種猜想因為鮑麥斯特伯爵家的到來已經變得毫無意義了。

「因為魔力有剩餘她們才會想些多餘的事情，那麼就暫時讓她們一直進行開墾作業好了。真的是，真希望她們能明白【迄今為止都重複著比男人還男人言行的女人，突然想擺出一副女人樣根本毫無意義】這種事呢。因為當家大人可是要做涼子大人和我的丈夫的人」

「哦、喔……」

不由得做出這種既不是肯定也不是否定的回應了。

為了島上統治的安定，我應該娶身為這座島總代官的涼子和副總代官的雪為妻，再讓她們生下和我的孩子繼承者兩個職位。這些事的重要程度我也明白。

然而，如果要我直截了當評價的話，這種做法總有些我無法釋然的部分在裡面。

「現在優先的是平定南方哦。雪小姐」

「當然，那方面的計劃已經在進行中了。唯殿下也知道這事吧？」

「確實如此呢」

松永久秀的女兒唯小姐也加入了我和雪的會話。

雪目前正在編成統治所必須的官僚、代官組織。

這座島的首領雖然是繼承了秋津洲高臣之名的涼子，但神官的她基本上就只是個裝飾品。

比起實際的統治，涼子負責的主要是復興這座島上已經荒廢了的類似神道的宗教和神社的工作，之後她將作為這種宗教的首領成為統治這座島的精神支柱。

如果把教會這種宗教強行引進會成為新騷動的根源，所以我才採取了和瑞穗公爵領一樣的做法。

這種時候，有霍恩海姆樞機卿可以商量真是幫了大忙。

反正短時間內鮑麥斯特伯爵家以外的人都被禁止進入這座島，所以不用擔心有強行來這裡布教的神官出現吧。

因為這些緣故，平日裡涼子都會留在大津城附近已經荒廢的大神社舉行秋津洲家代代相傳的神事，或者用治癒魔法為幫人治療。

由於傷員不斷涌來，所以那座大神社也正在進行大規模的改造施工。

雪擔當家主的細川家負責實際政務，松永家出身的唯小姐則拿到了輔佐副總代官雪的職務。

唯小姐雖然也不是魔法使，但因為是松永家的獨生女所以受過高度教育，也是非常優秀的人才。

除了唯小姐外，還有很多中央區域的代官和官僚們輔佐雪。

他們都在統治這座島的中央官僚階層中有了自己的一席之位。

另外，迄今為止我平定的那些地域也有各自的地域負責人。

也就是北部伊達家，中央三好家，西部十河家，東部北條家這樣的配置。

在這幾家之下還有從領主轉職成當地代官的那一群人，順利的話一個金字塔形統治組織便能就此完成。

不過我對這些都不是很懂，所以就全交給雪包辦了。

另外，這個組織中還有很多七條兼仲這樣不適合當領主的，或者乾脆以不想幹為由從中央官僚或武官轉業的人在。

兼仲現在成了改編後的中央治安維持用警備隊的負責人，除了訓練和巡邏外，偶爾他還會幫忙做些擴張城鎮工程什麼的。

松永久秀也華麗的轉型成了中央官僚層中的大人物，在雪的手下施展著他的辣腕手段。

唯小姐雖然以【雪是女性，身邊有個同樣是女性的部下跟著比較好】的理由成了雪的輔佐，但這二人的關係似乎不怎麼好。

雖然這二人做起工作來總能很有效率呃的完成……。

「平定南部後，就是怎麼提升開發效率的問題了。既然松永久秀殿下這麼優秀，那要不要做南部地域的責任人？至於唯殿下的夫婿，從南部的強力領主中挑選就行了吧」

「松永家的基礎在中央。就算突然成為南部地域的責任人，當地的人會不會聽我們的話也很讓人擔心。另外，關於松永家的夫婿這件事父親似乎自有他的打算，所以就無須雪小姐再多操心了」

這兩人的關係果然不好。

雪想方設法的把松永家送去南部，唯小姐則堂堂正正的否定了她。

即便看到對方露出『別對松永家的事亂插嘴，小丫頭！』的表情，也依舊一臉滿不在乎的雪也是不得了的大人物。

「松永家也想要島外的血脈？」

「說不定是那樣，但這可不是雪小姐該操心的事哦。畢竟，一切都要看當家大人的決定」

說完這句話後，唯小姐露出妖艷的笑容看著我。

唯小姐一方面工作起來比男人還厲害，一方面又和那三個DQN不同平時就很注意打扮。

雖然並不誇張但她的打扮很有品味，這座島上明明沒有香水，可她身上卻總帶著雖微弱卻味道很好的香氣。

不管在誰眼中唯小姐都是位漂亮的大姐姐，動作更是幹練到讓人想不到她只比我年長一歲。

雪雖然也會做符合女性的打扮，但那種風格更適合窩在室內裡，這一定是她出身北部的影響吧。

總之在女性的打扮方面，唯小姐這邊比較占優勢。

「當家大人是統治這座島的存在。所以要給予誰寵愛是他的自由。雖然為了副總代官的正統性而追求當家大人的血統這種做法並沒有錯，但因此就採取無視當家大人的心情逼迫他娶自己的做法就太庸俗了呢。我完全模仿不來的呀」

「嘖！」

因為唯小姐的指摘正中核心，雪咬牙嘖了一聲。所以我速速從這個地方溜走也是沒辦法的吧。

「總之就是這樣。女人好恐怖」

「想聽這種事的我也很恐怖哦。所以也沒法對她們多說什麼了」

無法忍耐辦公室劍張弩拔空氣而溜出來的我，和正在城堡中庭練習槍法的伊娜聊起了剛才發生的事。

「唯小姐看來也想生威爾的孩子呢。那個叫久秀的人，也給人種正在謀劃什麼的感覺」

因為他的名字是松永久秀嘛。

雖然過去是天下之主三好長慶的心腹，但久秀原本不是領主階級出身。

他完全是只靠自己的實力獲了三好家重臣之位和松永領領土的。

現在雖失去了領地，卻又得到了舊松永領代官兼雪的輔助這樣重要的職位。

因為久秀很優秀，所以把工作交給他確實會很輕鬆。

「我覺得松永先生盯上的，應該是雪小姐的副總代官職位」

「果然如此嗎……」

讓自己的獨生女唯小姐和我之間的孩子擔任下任家主的話，松永家就有了對抗獲得鮑麥斯特伯爵家血統的細川家的資本。

至於秋津洲家反正是裝飾用的，就算對抗也毫無意義。

光是他把細川家作為目標這件事，就能讓我明白久秀大叔是個讓人大意不得的人物了。

而且，這還不是武力鬥爭。

只要不會拖統治的後腿，派系鬥爭這種事就不能成為懲罰的理由

正因為久秀一直以來都生存在三好長慶之下的權謀世界，所以如今的體制反而更讓他有可能出人頭地嗎。

至少，松永家打算成為和細川家爭奪副總代官職位的對手這個意圖是已經很明顯了。

「我該怎麼辦？」

「那可不是我能決定的事」

「沒錯呢。這種事只能威爾自己決定」

露易絲也加入了我們的對話。

「人家這些妻子們可沒什麼決定權」

「對，就算威爾大人娶一百個妻子我們也不會說什麼」

卡琪婭和維爾瑪雖然也加入了進來，可所有人都把到底該娶誰這個問題推給了我決定。

「這種程度的事要自己決定才能算是貴族呀」

「親愛的你是鮑麥斯特伯爵家的初代家主，所以家主實力尤為強大。基本上，就連羅德里希先生也無法違背親愛的你的意思哦。所謂實力雄厚的家主就是這麼一回事」

連卡特莉娜和埃莉絲都這麼叮囑我。結果我只好去找最後的希望泰蕾莎。

至于莉薩，因為要監視DQN三人組所以人不在這裡。

「什麼？這還需要妾身的建議嗎？」

「幫幫我！泰蕾莎！」

在心中我甚至都稱呼她『泰蕾莎衛門』了。

（譯注：就是多啦A夢裡大雄每次求助的那個口氣）

「一切隨你喜歡……不過，這就和其他人一樣了吶。那麼妾身就闡述下自己的見解吧。妾身覺得娶涼子、雪、唯這三人都娶為妻子的方法比較好」

「理由呢？」

「威德林。身為為政者，要總是將最糟糕的預想當做未來來考慮才可以」

「最糟糕的預想？」

「秋津洲家還好。那個家只是裝飾，是融合了宗教方面權威才立於這座島頂點的。老實說，只要能在支撐起秋津洲家，不管誰來當副首領都可以。最理想的情況，是由複數有能力的家族定期交替著擔當這個職務」

也就是說，由細川家一家世襲副總代官職務這種做法會孕育出很大危險嗎。

「雪的確非常優秀，但她的孩子呢？孫子呢？重孫呢？這些人中只要出現一名惡劣愚蠢的家主，這座島的統治就會陷入不安定」

「那種時候就該松永家出場了嗎？」

「不錯。細川家不行的話，就讓松永家取而代之。這種權限鮑麥斯特伯爵家是擁有的，而且這也是為了防止秋津洲島陷入混亂。這邊人的不滿？鮑麥斯特伯爵家的力量是壓倒性的。如果這邊有抱怨，那麼拿出擊潰那些人的態度好了」

「嗚哇啊。好恐怖呢」

「露易絲啊。統治者基本上都是散布恐怖的存在吶。上面的人被小看的話下面的人就會混亂，最後受苦的就是最下面的領民們了」

那種事，我們在帝國內亂中已經體驗到煩了呢。

雖然現在進行的很順利，但這並不代表對這座島的統治就可以採取放任主義。

不過，誰嫁給我這種事還是希望他們過後再搞啊。

「那三人的話就沒問題。但伊達家的小丫頭就說什麼也不行了。和那個小丫頭聯姻可是一步臭棋吶」

「如果家主繼承了威德林先生的血統，北部就可能採取反抗中央的態度了呢」

舊領主們雖然全都轉職成了代官，但他們在自己負責的原自家領地中都還留有一定影響力。

如果藤子和我的孩子當了伊達家家主，那麼北部就有可能和中央對立導致島的統治發生混亂。

「雖然現在還只是還不知男女情事的孩子的戲言，但你還是適當拒絕下吧」

「那露露呢？」

「那孩子沒問題。就算她成為威德林的妻子，也不會產生什麼多餘的瓜葛。就算產生了，也不過就是個移民村落的級別吧？只要讓威德林和她的孩子擔任那個地域的代官，其他人就會老實服從了」

話說，泰蕾莎居然能想的這麼遠啊，不愧是前公爵。

「姆姆姆，那種話俺可不能當做沒聽到！」

「噢噢，這不是剛才提到的小丫頭嘛」

「別把俺當成小丫頭！不過就是身高稍微高些胸部稍微大些而已。等俺到了泰蕾莎殿下這個年紀，也會變成不相上下的完美身材！」

看起來，泰蕾莎剛才似乎是為了特意叮囑藤子才說那番話給她聽的。

雖然我覺得以小孩子為對象這麼提意見的泰蕾莎太沒大人樣了些，但藤子身上有著超出年齡的成熟部分。

泰蕾莎好像也是知道藤子能理解這些才叮囑她的。

「露露，會成為威德林大人的好妻子的」

「哦，自由去做吧。因為露露這邊沒有任何問題。而且你喜歡威德林吧？」

「是的，非常喜歡」

「是嗎，那就是兩情相悅了吶」

對泰蕾莎的問題，露露滿臉笑容的回答了。

只是就算被五歲的LOLI說最喜歡了，我也只會產生父親被女兒說最喜歡的那種感覺。

「俺和露露有很大差別嗎！俺也最喜歡威德林了！」

「可是你的出身太糟了。無法選擇生在哪家的本人雖然無罪，但那影響還是會一聲纏著你的。放棄吧」

「嗚！因為俺是伊達家下任家主所以才不行嗎？」

「就是這麼回事。不是因為你的身心有問題。這點我可以向你保證」

因為對方雖然只有五歲但能理解其中緣由，所以泰蕾莎也毫不留情。

反過來說，這也意味著她認同藤子是和自己對等的存在。

「那麼我就做伊達藤子。不，我要捨棄伊達之名只做藤子！」

「你是認真的嗎？」

「這和泰蕾莎殿下捨棄了菲利普公爵爵位和家世的做法相同不是嗎。其實，昨天父親大人那邊傳來了母親大人又有了身孕的聯絡。那麼正好，俺就趁機告訴他們想家督位子讓給那個孩子吧。如果下一個孩子是男孩的話，家臣們也不會對俺捨棄家督位子的行為多說什麼了」

藤子的嘴真是巧到讓人無法想像她只有五歲。

繼承家督這件事，原本就是在伊達家直系子女只有藤子一個當代政宗又疾病纏身這種情況下不得已做出的決定。

現在政宗已經恢復了健康，又能和妻子生小孩了。

所以在並非戰亂之世的當下，就沒必要勉強藤子繼承家督之位了嗎。

「原來如此。你不惜捨棄家族也要成為威德林的妻子？」

「對啊。因為比起伊達家當家大人更重要！」

「既然有這樣的覺悟，那就完全沒有問題了吶」

「泰蕾莎殿下真明白事理」

喂，泰蕾莎。

不要擅自給我增加妻子啊。

「太好了呢，威德林。年輕妻子增加了」

「咔啊———！」

「當家大人。俺會和露露兩人當個好妻子的」

如果現在拒絕的話似乎會弄哭藤子，所以我什麼也說不出口。

「等———！那我們該怎麼辦？」

「就是說啊！對我們來說那可只是距今一兩年後的事！」

「老師您不會拋棄我們的吧？」

「說什麼拋棄不拋棄的。阿格妮絲你們不是我的弟子……」

「「「是婚約者哦？」」」

這下就八人了……。

我和這座與瑞穗人有著相同祖先的秋津洲島之間，搞不好有著什麼完全出乎我意料之外的聯繫存在。

我現在也只能靠擅自思考這些來逃避現實了……。
